"use strict";
var MatcherUnitarioMatcherAdapter = (function () {
    function MatcherUnitarioMatcherAdapter(matcher) {
        this.matcher = matcher;
    }
    MatcherUnitarioMatcherAdapter.prototype.matches = function (entrada) {
        var match = this.matcher.match(entrada);
        if (match === undefined)
            return [];
        return [match];
    };
    return MatcherUnitarioMatcherAdapter;
}());
exports.MatcherUnitarioMatcherAdapter = MatcherUnitarioMatcherAdapter;
var MatcherAutomato = (function () {
    function MatcherAutomato(automato) {
        this.automato = automato;
        this.estadoInicial = automato.estado;
    }
    MatcherAutomato.prototype.matches = function (entrada) {
        var palavras = [];
        var palavra = '';
        this.automato.estado = this.estadoInicial;
        for (var i = 0; i < entrada.length; i++) {
            try {
                var letra = entrada[i];
                this.automato.avancar(letra);
                palavra = palavra + letra;
            }
            catch (ex) {
                if (this.automato.isEstadoFinal()) {
                    palavras.push(palavra);
                }
                if (palavra !== '') {
                    this.automato.estado = this.estadoInicial;
                    palavra = '';
                    i--;
                }
            }
        }
        return palavras;
    };
    return MatcherAutomato;
}());
exports.MatcherAutomato = MatcherAutomato;
//# sourceMappingURL=matcher.js.map