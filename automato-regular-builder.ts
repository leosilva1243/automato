import { Automato } from './automato'

export class AutomatoRegularBuilder {
    private estado: string = '0'
    private infinito = false
    private padrao?: string
    private parent?: AutomatoRegularBuilder

    private aplicar(automato: Automato) {
        if (this.parent) {
            this.parent.aplicar(automato)
        }
        automato.adicionarEstado(this.estado)
        if (this.infinito) {
            automato.adicionarTransicao(
                this.estado, 
                this.estado, 
                this.padrao!)
        }
        if (this.parent) {
            automato.adicionarTransicao(
                this.parent.estado,
                this.estado,
                this.padrao!)
        }
    }

    // [TODO] Implementar repeticoes = 0
    adicionar(padrao: string, repeticoes: number = 1, infinito: boolean = false): AutomatoRegularBuilder {
        let builder = this.criarFilho(padrao)
        while (repeticoes > 1) {
            builder = builder.criarFilho(padrao)
            repeticoes--
        }
        builder.infinito = infinito
        return builder
    }

    private criarFilho(padrao: string): AutomatoRegularBuilder {
        const estado = (+this.estado + 1) + ''
        const builder = new AutomatoRegularBuilder()
        builder.estado = estado
        builder.padrao = padrao
        builder.parent = this
        return builder

    }

    build(): Automato {
        const automato = new Automato()
        this.aplicar(automato)
        automato.setEstadoFinal(this.estado, true)
        automato.estado = automato.estados[0]
        return automato
    }
}