export interface MatcherUnitario {
    match(entrada: string): string | undefined;
}

export class QualquerValorMatcher implements MatcherUnitario {
    match(entrada: string): string | undefined {
        return entrada
    }
}

export class ValorMatcher implements MatcherUnitario {
    constructor(
        readonly valor: string
    ) { }

    match(entrada: string): string | undefined {
        if (entrada === this.valor) return entrada
        return undefined
    }
}

export class IntervaloMatcher implements MatcherUnitario {
    constructor(
        readonly inicio: string,
        readonly fim: string
    ) { }

    match(entrada: string): string | undefined {
        if (entrada.length !== 1) return undefined
        if (this.inicio <= entrada && entrada <= this.fim) {
            return entrada
        }
        return undefined
    }
}

export class PrimeiroMatcher implements MatcherUnitario {
    constructor(
        readonly matchers: MatcherUnitario[]
    ) { }

    match(entrada: string): string | undefined {
        for (const matcher of this.matchers) {
            const match = matcher.match(entrada)
            if (match !== undefined) return match
        }
        return undefined
    }
}