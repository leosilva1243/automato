import { 
    MatcherUnitario, PrimeiroMatcher, IntervaloMatcher, ValorMatcher, QualquerValorMatcher 
} from './matcher-unitario'

export class MatcherUnitarioFactory {
    i18N: MatcherUniaoFactoryI18N = new MatcherUniaoFactoryI18NPortugues()

    criarMatcher(entrada: string): MatcherUnitario {
        let matchers: MatcherUnitario[] = []
        while (entrada.length > 0) {
            if (entrada[0] === '\\') {
                if (entrada.length > 1) {
                    matchers.push(new ValorMatcher(entrada[1]))
                }
                entrada = entrada.slice(2)
            }
            else {
                if (entrada.length > 1 && entrada[1] === '-') {
                    const inicio = entrada[0]
                    const fim = entrada[2]
                    matchers.push(new IntervaloMatcher(inicio, fim))
                    entrada = entrada.slice(3)
                }
                else if (entrada[0] === '.') {
                    matchers.push(new QualquerValorMatcher())
                    entrada = entrada.slice(1)
                }
                else {
                    let padrao = entrada[0]
                    if (padrao === '(') {
                        const indiceFechamento = entrada.indexOf(')')
                        if (indiceFechamento === -1) {
                            throw this.i18N.parenteseAberto()
                        }
                        padrao = entrada.slice(1, indiceFechamento)
                        entrada = entrada.slice(indiceFechamento)
                    }
                    matchers.push(new ValorMatcher(padrao))
                    entrada = entrada.slice(1)
                }
            }
        }
        if (matchers.length === 1) return matchers[0]
        return new PrimeiroMatcher(matchers)
    }
}

export interface MatcherUniaoFactoryI18N {
    parenteseAberto(): string
}

export class MatcherUniaoFactoryI18NPortugues implements MatcherUniaoFactoryI18N {
    parenteseAberto(): string {
        return 'Algum parêntese ficou aberto.'
    }
}