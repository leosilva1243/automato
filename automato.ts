import { MatcherUnitarioFactory } from './matcher-unitario-factory'
import {
    MatcherUnitario, PrimeiroMatcher, IntervaloMatcher, ValorMatcher
} from './matcher-unitario'

export class Automato {
    private estadosAlterados = false
    private copiaEstados: string[] = []
    private estadosFinaisAlterados = false
    private copiaEstadosFinais: string[] = []

    private tabela_transicoes: Transicao[][] = [];
    private _estados: string[] = []
    private _estadosFinais: number[] = []
    private _estado?: number

    i18N: AutomatoI18N = new AutomatoI18NPortugues()
    private readonly matcherFactory = new MatcherUnitarioFactory()

    get estados(): string[] {
        if (this.estadosAlterados) {
            this.copiaEstados = this._estados.slice()
            this.estadosAlterados = false
        }
        return this.copiaEstados
    }

    get estadosFinais(): string[] {
        if (this.estadosFinaisAlterados) {
            this.copiaEstadosFinais = this._estadosFinais
                .map(indice => this._estados[indice])
            this.estadosFinaisAlterados = false
        }
        return this.copiaEstadosFinais
    }

    get estado(): string | undefined {
        if (this._estado === undefined) return undefined
        return this._estados[this._estado]
    }

    set estado(estado: string | undefined) {
        if (estado === undefined) {
            delete this._estado
            return
        }
        const indice = this._estados.indexOf(estado)
        if (indice === -1) {
            delete this._estado
            return
        }
        this._estado = indice
    }

    adicionarEstado(nome: string) {
        const indice = this._estados.indexOf(nome)
        if (indice != -1) throw this.i18N.jaExisteEstado(nome)
        this._estados.push(nome)
        this.tabela_transicoes.push([])
        this.estadosAlterados = true
    }

    removerEstado(nome: string) {
        const indice = this._estados.indexOf(nome)
        if (indice === -1) throw this.i18N.naoExisteEstado(nome)
        if (this.estado === nome) {
            delete this.estado
        }
        if (this.isEstadoFinal(nome)) {
            this.setEstadoFinal(nome, false)
        }
        this._estados.splice(indice, 1)
        this.tabela_transicoes.splice(indice, 1)
        this.estadosAlterados = true
    }

    setEstadoFinal(nome: string, final: boolean) {
        const indice = this._estados.indexOf(nome)
        if (indice === -1) throw this.i18N.naoExisteEstado(nome)
        const indiceEstadoFinal = this._estadosFinais.indexOf(indice)
        if (final) {
            if (indiceEstadoFinal !== -1) throw this.i18N.estadoJaFinal(nome)
            this._estadosFinais.push(indice)
        }
        else {
            if (indiceEstadoFinal === -1) throw this.i18N.estadoNaoFinal(nome)
            this._estadosFinais.splice(indiceEstadoFinal, 1)
        }
        this.estadosFinaisAlterados = true
    }

    isEstadoFinal(nome?: string): boolean {
        nome = nome || this.estado
        if (nome === undefined) throw this.i18N.estadoNaoDefinido()
        const indice = this._estados.indexOf(nome)
        if (indice === -1) throw this.i18N.naoExisteEstado(nome)
        return this._estadosFinais.indexOf(indice) !== -1
    }

    adicionarTransicao(estado: string, novoEstado: string, padrao: string) {
        const indiceEstado = this._estados.indexOf(estado)
        if (indiceEstado === -1) throw this.i18N.naoExisteEstado(estado)
        const indiceNovoEstado = this._estados.indexOf(novoEstado)
        if (indiceNovoEstado === -1) throw this.i18N.naoExisteEstado(novoEstado)
        const matcher = this.matcherFactory.criarMatcher(padrao)
        const transicao = new Transicao(matcher, indiceNovoEstado)
        this.tabela_transicoes[indiceEstado].push(transicao)
    }

    avancar(entrada: string) {
        if (this._estado === undefined) throw this.i18N.estadoNaoDefinido()
        const transicoes = this.tabela_transicoes[this._estado]
        for (const transicao of transicoes) {
            if (transicao.matcher.match(entrada) !== undefined) {
                this._estado = transicao.novoEstado
                return
            }
        }
        throw this.i18N.nenhumaTransicao(entrada)
    }
}

class Transicao {
    constructor(
        readonly matcher: MatcherUnitario,
        readonly novoEstado: number
    ) { }
}

export interface AutomatoI18N {
    naoExisteEstado(estado: string): string
    jaExisteEstado(estado: string): string
    estadoJaFinal(estado: string): string
    estadoNaoFinal(estado: string): string
    estadoNaoDefinido(): string
    nenhumaTransicao(entrada: string): string
}

export class AutomatoI18NPortugues implements AutomatoI18N {
    naoExisteEstado(estado: string): string {
        return `Não existe estado com o nome ${estado}.`
    }

    jaExisteEstado(estado: string): string {
        return `Já existe estado com o nome ${estado}.`
    }

    estadoJaFinal(estado: string): string {
        return `Estado com o nome ${estado} já é final.`
    }

    estadoNaoFinal(estado: string): string {
        return `Estado com o nome ${estado} não é final.`
    }

    estadoNaoDefinido(): string {
        return `Estado atual não definido.`
    }

    nenhumaTransicao(entrada: string): string {
        return `Nenhuma transição encontrada para a entrada ${entrada}.`
    }
}