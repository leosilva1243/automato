import * as assert from 'assert'

import { Automato } from '../automato'
import { MatcherUnitarioFactory } from "../matcher-unitario-factory";

describe('Automato', () => {
    let automato: Automato

    beforeEach(() => {
        automato = new Automato()
    })

    describe('adicionarEstado', () => {
        it('deveria conter um estado após adicionado', () => {
            automato.adicionarEstado('q0')
            assert.equal(automato.estados.length, 1)
        })

        it('deveria conter o estado após adicionado', () => {
            automato.adicionarEstado('q0')
            assert.notEqual(automato.estados.indexOf('q0'), -1)
        })

        it('deveria lançar um erro quando tentar adicionar estado com mesmo nome', () => {
            automato.adicionarEstado('q0')
            assert.throws(() => automato.adicionarEstado('q0'))
        })
    })

    describe('removerEstado', () => {
        it('deveria conter um estado após removido', () => {
            automato.adicionarEstado('q0')
            automato.adicionarEstado('q1')
            automato.removerEstado('q0')
            assert.equal(automato.estados.length, 1)
        })

        it('deveria não conter o estado após removido', () => {
            automato.adicionarEstado('q0')
            automato.removerEstado('q0')
            assert.equal(automato.estados.indexOf('q0'), -1)
        })

        it('deveria lançar um erro quando tentar remover um estado inexistente', () => {
            automato.adicionarEstado('q0')
            automato.adicionarEstado('q1')
            assert.throws(() => automato.removerEstado('q2'))
        })
    })

    describe('setEstadoFinal', () => {
        it('deveria conter o estado final após setado', () => {
            automato.adicionarEstado('q0')
            automato.setEstadoFinal('q0', true)
            assert.notEqual(automato.estadosFinais.indexOf('q0'), -1)
        })

        it('deveria ser estado final após setado', () => {
            automato.adicionarEstado('q0')
            automato.setEstadoFinal('q0', true)
            assert.ok(automato.isEstadoFinal('q0'))
        })

        it('deveria não ser estado final após setado', () => {
            automato.adicionarEstado('q0')
            automato.setEstadoFinal('q0', true)
            automato.setEstadoFinal('q0', false)
            assert.ok(!automato.isEstadoFinal('q0'))
        })
    })
})