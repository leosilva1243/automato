"use strict";
var assert = require('assert');
var automato_1 = require('../automato');
describe('Automato', function () {
    var automato;
    beforeEach(function () {
        automato = new automato_1.Automato();
    });
    describe('adicionarEstado', function () {
        it('deveria conter um estado após adicionado', function () {
            automato.adicionarEstado('q0');
            assert.equal(automato.estados.length, 1);
        });
        it('deveria conter o estado após adicionado', function () {
            automato.adicionarEstado('q0');
            assert.notEqual(automato.estados.indexOf('q0'), -1);
        });
        it('deveria lançar um erro quando tentar adicionar estado com mesmo nome', function () {
            automato.adicionarEstado('q0');
            assert.throws(function () { return automato.adicionarEstado('q0'); });
        });
    });
    describe('removerEstado', function () {
        it('deveria conter um estado após removido', function () {
            automato.adicionarEstado('q0');
            automato.adicionarEstado('q1');
            automato.removerEstado('q0');
            assert.equal(automato.estados.length, 1);
        });
        it('deveria não conter o estado após removido', function () {
            automato.adicionarEstado('q0');
            automato.removerEstado('q0');
            assert.equal(automato.estados.indexOf('q0'), -1);
        });
        it('deveria lançar um erro quando tentar remover um estado inexistente', function () {
            automato.adicionarEstado('q0');
            automato.adicionarEstado('q1');
            assert.throws(function () { return automato.removerEstado('q2'); });
        });
    });
    describe('setEstadoFinal', function () {
        it('deveria conter o estado final após setado', function () {
            automato.adicionarEstado('q0');
            automato.setEstadoFinal('q0', true);
            assert.notEqual(automato.estadosFinais.indexOf('q0'), -1);
        });
        it('deveria ser estado final após setado', function () {
            automato.adicionarEstado('q0');
            automato.setEstadoFinal('q0', true);
            assert.ok(automato.isEstadoFinal('q0'));
        });
        it('deveria não ser estado final após setado', function () {
            automato.adicionarEstado('q0');
            automato.setEstadoFinal('q0', true);
            automato.setEstadoFinal('q0', false);
            assert.ok(!automato.isEstadoFinal('q0'));
        });
    });
});
//# sourceMappingURL=automato.js.map