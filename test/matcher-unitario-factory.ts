import { assert } from 'chai'
import { MatcherUnitarioFactory } from "../matcher-unitario-factory"
import { 
    ValorMatcher, PrimeiroMatcher, IntervaloMatcher, QualquerValorMatcher
} from '../matcher-unitario'

describe('MatcherUniaoFactory', () => {
    describe('criarMatcher', () => {
        it('deveria criar ValorMatcher', () => {
            const factory = new MatcherUnitarioFactory()

            const resultadoA = factory.criarMatcher('a')
            const resultadoB = factory.criarMatcher('(ab)')
            const resultadoC = factory.criarMatcher('\\-')
            const resultadoD = factory.criarMatcher('\\.')

            assert.instanceOf(resultadoA, ValorMatcher)
            assert.instanceOf(resultadoB, ValorMatcher)
            assert.instanceOf(resultadoC, ValorMatcher)
            assert.instanceOf(resultadoD, ValorMatcher)
        })

        it('deveria criar IntervaloMatcher', () => {
            const factory = new MatcherUnitarioFactory()

            const resultadoA = factory.criarMatcher('a-z')
            const resultadoB = factory.criarMatcher('A-Z')
            const resultadoC = factory.criarMatcher('0-9')

            assert.instanceOf(resultadoA, IntervaloMatcher)
            assert.instanceOf(resultadoB, IntervaloMatcher)
            assert.instanceOf(resultadoC, IntervaloMatcher)
        })

        it('deveria criar QualquerValorMatcher', () => {
            const factory = new MatcherUnitarioFactory()

            const resultado = factory.criarMatcher('.')

            assert.instanceOf(resultado, QualquerValorMatcher)
        })

        it('deveria criar MatcherUniao', () => {
            const factory = new MatcherUnitarioFactory()

            const resultadoA = factory.criarMatcher('aa')
            const resultadoB = factory.criarMatcher('(aa)a')
            const resultadoC = factory.criarMatcher('(aa)(aa)')
            const resultadoD = factory.criarMatcher('a.')
            const resultadoE = factory.criarMatcher('aa-z')

            assert.instanceOf(resultadoA, PrimeiroMatcher)
            assert.instanceOf(resultadoB, PrimeiroMatcher)
            assert.instanceOf(resultadoC, PrimeiroMatcher)
            assert.instanceOf(resultadoD, PrimeiroMatcher)
            assert.instanceOf(resultadoE, PrimeiroMatcher)
        })

        it('deveria ter valor (ValorMatcher)', () => {
            const factory = new MatcherUnitarioFactory()

            const resultadoA = factory.criarMatcher('a') as ValorMatcher
            const resultadoB = factory.criarMatcher('(ab)') as ValorMatcher
            const resultadoC = factory.criarMatcher('\\-') as ValorMatcher
            const resultadoD = factory.criarMatcher('\\.') as ValorMatcher

            assert.equal(resultadoA.valor, 'a')
            assert.equal(resultadoB.valor, 'ab')
            assert.equal(resultadoC.valor, '-')
            assert.equal(resultadoD.valor, '.')
        })

        it('deveria ter intervalo (IntervaloMatcher)', () => {
            const factory = new MatcherUnitarioFactory()

            const resultadoA = factory.criarMatcher('a-z') as IntervaloMatcher
            const resultadoB = factory.criarMatcher('A-Z') as IntervaloMatcher
            const resultadoC = factory.criarMatcher('0-9') as IntervaloMatcher

            assert.equal(resultadoA.inicio, 'a')
            assert.equal(resultadoA.fim, 'z')
            assert.equal(resultadoB.inicio, 'A')
            assert.equal(resultadoB.fim, 'Z')
            assert.equal(resultadoC.inicio, '0')
            assert.equal(resultadoC.fim, '9')
        })

        it('deveria ter tantos matchers (PrimeiroMatcher)', () => {
            const factory = new MatcherUnitarioFactory()

            const matcherA = factory.criarMatcher('aa') as PrimeiroMatcher
            const matcherB = factory.criarMatcher('(aa)a') as PrimeiroMatcher
            const matcherC = factory.criarMatcher('(aa)(aa)') as PrimeiroMatcher
            const matcherD = factory.criarMatcher('(aa)aa') as PrimeiroMatcher
            const matcherE = factory.criarMatcher('(aa)a(aa)') as PrimeiroMatcher
            const matcherF = factory.criarMatcher('aa.aa') as PrimeiroMatcher
            const matcherG = factory.criarMatcher('aa\\.aa') as PrimeiroMatcher
            const matcherH = factory.criarMatcher('aa\\-aa') as PrimeiroMatcher
            const matcherI = factory.criarMatcher('aa-zaaa') as PrimeiroMatcher

            const resultadoA = matcherA.matchers.length
            const resultadoB = matcherB.matchers.length
            const resultadoC = matcherC.matchers.length
            const resultadoD = matcherD.matchers.length
            const resultadoE = matcherE.matchers.length
            const resultadoF = matcherF.matchers.length
            const resultadoG = matcherG.matchers.length
            const resultadoH = matcherH.matchers.length
            const resultadoI = matcherI.matchers.length

            assert.equal(resultadoA, 2)
            assert.equal(resultadoB, 2)
            assert.equal(resultadoC, 2)
            assert.equal(resultadoD, 3)
            assert.equal(resultadoE, 3)
            assert.equal(resultadoF, 5)
            assert.equal(resultadoG, 5)
            assert.equal(resultadoH, 5)
            assert.equal(resultadoI, 5)
        })

        it('deveria ter tais matchers (PrimeiroMatcher)', () => {
            const factory = new MatcherUnitarioFactory()

            const matcherA = factory.criarMatcher('aa') as PrimeiroMatcher
            const matcherB = factory.criarMatcher('(aa)a') as PrimeiroMatcher
            const matcherC = factory.criarMatcher('(aa)(aa)') as PrimeiroMatcher
            const matcherD = factory.criarMatcher('a.') as PrimeiroMatcher
            const matcherE = factory.criarMatcher('aa-z') as PrimeiroMatcher

            const matchersA = matcherA.matchers
            const matchersB = matcherB.matchers
            const matchersC = matcherC.matchers
            const matchersD = matcherD.matchers
            const matchersE = matcherE.matchers

            assert.instanceOf(matchersA[0], ValorMatcher)
            assert.instanceOf(matchersA[1], ValorMatcher)
            assert.instanceOf(matchersB[0], ValorMatcher)
            assert.instanceOf(matchersB[1], ValorMatcher)
            assert.instanceOf(matchersC[0], ValorMatcher)
            assert.instanceOf(matchersC[1], ValorMatcher)
            assert.instanceOf(matchersD[0], ValorMatcher)
            assert.instanceOf(matchersD[1], QualquerValorMatcher)
            assert.instanceOf(matchersE[0], ValorMatcher)
            assert.instanceOf(matchersE[1], IntervaloMatcher)
        })
    })
})