
import { assert } from 'chai'

import { Matcher, MatcherAutomato } from '../matcher'
import { AutomatoRegularBuilder } from '../automato-regular-builder'
import { MatcherRegularFactory } from '../matcher-regular-factory'

describe('MatcherRegularFactory', () => {
    describe('criarMatcher', () => {
        const regex = new MatcherRegularFactory().criarMatcher
        const builder = new AutomatoRegularBuilder()

        it('deveria ser equivalente (qualquer valor)', () => {
            const automato = builder
                .adicionar('.')
                .build()
            const matcherA = regex('.') 
            const matcherB = new MatcherAutomato(automato)

            assert.deepEqual(matcherA, matcherB)
        })
    })
})