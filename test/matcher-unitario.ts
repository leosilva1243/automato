import * as assert from 'assert'
import { 
    ValorMatcher, IntervaloMatcher, PrimeiroMatcher, QualquerValorMatcher
} from "../matcher-unitario"

describe('QualquerValorMatcher', () => {
    describe('match', () => {
        it('deveria dar match', () => {
            const matcher = new QualquerValorMatcher()
            const resultadoA = matcher.match('a')
            const resultadoB = matcher.match('0')
            const resultadoC = matcher.match('abc')
            assert.equal(resultadoA, 'a')
            assert.equal(resultadoB, '0')
            assert.equal(resultadoC, 'abc')
        })
    })
})

describe('ValorMatcher', () => {
    describe('match', () => {
        it('deveria dar match (valor unico)', () => {
            const matcher = new ValorMatcher('a')
            const resultado = matcher.match('a')
            assert.notEqual(resultado, null)
        })

        it('não deveria dar match (valor único)', () => {
            const matcher = new ValorMatcher('a')
            const resultadoA = matcher.match('b')
            const resultadoB = matcher.match('A')
            assert.equal(resultadoA, null)
            assert.equal(resultadoB, null)
        })

        it('deveria dar match (valor múltiplo)', () => {
            const matcher = new ValorMatcher('leo')
            const resultadoA = matcher.match('leo')
            assert.equal(resultadoA, 'leo')
        })

        it('não deveria dar match (valor múltiplo)', () => {
            const matcher = new ValorMatcher('leo')
            const resultadoA = matcher.match('oel')
            const resultadoB = matcher.match('o')
            const resultadoC = matcher.match('l')
            const resultadoD = matcher.match('leoo')
            const resultadoE = matcher.match('oleo')
            assert.equal(resultadoA, null)
            assert.equal(resultadoB, null)
            assert.equal(resultadoC, null)
            assert.equal(resultadoD, null)
            assert.equal(resultadoE, null)
        })
    })
})

describe('IntervaloMatcher', () => {
    describe('match', () => {
        it('deveria dar match', () => {
            const matcher = new IntervaloMatcher('a', 'c')
            const resultadoA = matcher.match('a')
            const resultadoB = matcher.match('b')
            const resultadoC = matcher.match('c')
            assert.equal(resultadoA, 'a')
            assert.equal(resultadoB, 'b')
            assert.equal(resultadoC, 'c')
        })

        it('não deveria dar match', () => {
            const matcher = new IntervaloMatcher('a', 'c')
            const resultadoA = matcher.match('0')
            const resultadoB = matcher.match('d')
            const resultadoC = matcher.match('B')
            const resultadoD = matcher.match('abc')
            assert.equal(resultadoA, null)
            assert.equal(resultadoB, null)
            assert.equal(resultadoC, null)
            assert.equal(resultadoD, null)
        })
    })
})

describe('PrimeiroMatcher', () => {
    describe('match', () => {
        it('deveria dar match', () => {
            const matcherA = new ValorMatcher('a')
            const matcherB = new ValorMatcher('b')
            const matcherC = new ValorMatcher('c')
            const matchers = [matcherA, matcherB, matcherC]
            const matcher = new PrimeiroMatcher(matchers)
            const resultadoA = matcher.match('a')
            const resultadoB = matcher.match('b')
            const resultadoC = matcher.match('c')
            assert.equal(resultadoA, 'a')
            assert.equal(resultadoB, 'b')
            assert.equal(resultadoC, 'c')
        })

        it('não deveria dar match', () => {
            const matcherA = new ValorMatcher('a')
            const matcherB = new ValorMatcher('b')
            const matcherC = new ValorMatcher('c')
            const matchers = [matcherA, matcherB, matcherC]
            const matcher = new PrimeiroMatcher(matchers)
            const resultadoA = matcher.match('A')
            const resultadoB = matcher.match('d')
            const resultadoC = matcher.match('abc')
            assert.equal(resultadoA, null)
            assert.equal(resultadoB, null)
            assert.equal(resultadoC, null)
        })
    })
})