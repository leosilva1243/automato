"use strict";
require('es6-shim');
var request = require('request-promise');
var matcher_regular_factory_1 = require("./matcher-regular-factory");
var matcherRegularFactory = new matcher_regular_factory_1.MatcherRegularFactory();
var matcher = matcherRegularFactory.criarMatcher('/watch?v=[a-zA-Z0-9][a-zA-Z0-9_\\-]+');
var urls = [];
urls.push('https://www.youtube.com/watch?v=U55V1knQi8c&list=PLZ2hvx2z1WR63wAlfj5RVZhw0_3gwPdgT');
urls.push('https://www.youtube.com/watch?v=HqbJsA-WKr4&list=PLDfKAXSi6kUapi0I8jCbQK3_TeIiDNEw2');
urls.push('https://www.youtube.com/watch?v=_F-HhJ2uTFE&list=PL27Vn4lmhlTe6ulSJy9Kvwj-_prAOcr2O');
var url = urls[0];
// pesquisar sobre
request(url)
    .then(function (html) {
    console.log(matcher.matches(html));
})
    .catch(function (erro) {
    console.log('Deu algo errado com a requisição.');
});
//# sourceMappingURL=index.js.map