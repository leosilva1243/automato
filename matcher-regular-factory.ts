import { Matcher, MatcherAutomato } from './matcher'
import { AutomatoRegularBuilder } from './automato-regular-builder'

export class MatcherRegularFactory {
    i18N: MatcherRegularFactoryI18N = new MatcherRegularFactoryI18NPortugues()

    criarMatcher(entrada: string): Matcher {
        let automatoBuilder = new AutomatoRegularBuilder()
        while (entrada.length > 0) {
            if (entrada[0] === '\\') {
                if (entrada.length > 1) {
                    automatoBuilder = automatoBuilder
                        .adicionar(entrada[1])
                }
                entrada = entrada.slice(1)
            }
            else {
                let padrao = entrada[0]
                if (padrao === '[') {
                    const indiceFechamento = entrada.indexOf(']')
                    if (indiceFechamento === -1) {
                        throw this.i18N.colcheteAberto()
                    }
                    padrao = entrada.slice(1, indiceFechamento)
                    entrada = entrada.slice(indiceFechamento)
                }
                let infinito = false
                if (entrada.length > 1) {
                    infinito = entrada[1] === '+'
                    if (infinito) entrada = entrada.slice(1)
                }
                automatoBuilder = automatoBuilder
                    .adicionar(padrao, 1, infinito)
                entrada = entrada.slice(1)
            }
        }
        const automato = automatoBuilder.build()
        return new MatcherAutomato(automato)
    }
}

export interface MatcherRegularFactoryI18N {
    colcheteAberto(): string
}

export class MatcherRegularFactoryI18NPortugues implements MatcherRegularFactoryI18N {
    colcheteAberto(): string {
        return 'Algum colchete ficou aberto.'
    }
}