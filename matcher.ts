import { Automato } from "./automato";
import { MatcherUnitario } from "./matcher-unitario";

export interface Matcher {
    matches(entrada: string): string[]
}

export class MatcherUnitarioMatcherAdapter implements Matcher {
    constructor(
        readonly matcher: MatcherUnitario
    ) { }

    matches(entrada: string): string[] {
        const match = this.matcher.match(entrada)
        if (match === undefined) return []
        return [match]
    }
}

export class MatcherAutomato implements Matcher {
    private readonly estadoInicial: string

    constructor(
        readonly automato: Automato,
    ) { 
        this.estadoInicial = automato.estado!
    }

    matches(entrada: string): string[] {
        let palavras: string[] = []
        let palavra = ''
        this.automato.estado = this.estadoInicial
        for (let i = 0; i < entrada.length; i++) {
            try {
                const letra = entrada[i]
                this.automato.avancar(letra)
                palavra = palavra + letra
            }
            catch (ex) {
                if (this.automato.isEstadoFinal()) {
                    palavras.push(palavra)
                }
                if (palavra !== '') {
                    this.automato.estado = this.estadoInicial
                    palavra = ''
                    i--
                }
            }
        }
        return palavras
    }
}