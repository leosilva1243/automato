"use strict";
var matcher_unitario_factory_1 = require('./matcher-unitario-factory');
var Automato = (function () {
    function Automato() {
        this.estadosAlterados = false;
        this.copiaEstados = [];
        this.estadosFinaisAlterados = false;
        this.copiaEstadosFinais = [];
        this.tabela_transicoes = [];
        this._estados = [];
        this._estadosFinais = [];
        this.i18N = new AutomatoI18NPortugues();
        this.matcherFactory = new matcher_unitario_factory_1.MatcherUnitarioFactory();
    }
    Object.defineProperty(Automato.prototype, "estados", {
        get: function () {
            if (this.estadosAlterados) {
                this.copiaEstados = this._estados.slice();
                this.estadosAlterados = false;
            }
            return this.copiaEstados;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Automato.prototype, "estadosFinais", {
        get: function () {
            var _this = this;
            if (this.estadosFinaisAlterados) {
                this.copiaEstadosFinais = this._estadosFinais
                    .map(function (indice) { return _this._estados[indice]; });
                this.estadosFinaisAlterados = false;
            }
            return this.copiaEstadosFinais;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Automato.prototype, "estado", {
        get: function () {
            if (this._estado === undefined)
                return undefined;
            return this._estados[this._estado];
        },
        set: function (estado) {
            if (estado === undefined) {
                delete this._estado;
                return;
            }
            var indice = this._estados.indexOf(estado);
            if (indice === -1) {
                delete this._estado;
                return;
            }
            this._estado = indice;
        },
        enumerable: true,
        configurable: true
    });
    Automato.prototype.adicionarEstado = function (nome) {
        var indice = this._estados.indexOf(nome);
        if (indice != -1)
            throw this.i18N.jaExisteEstado(nome);
        this._estados.push(nome);
        this.tabela_transicoes.push([]);
        this.estadosAlterados = true;
    };
    Automato.prototype.removerEstado = function (nome) {
        var indice = this._estados.indexOf(nome);
        if (indice === -1)
            throw this.i18N.naoExisteEstado(nome);
        if (this.estado === nome) {
            delete this.estado;
        }
        if (this.isEstadoFinal(nome)) {
            this.setEstadoFinal(nome, false);
        }
        this._estados.splice(indice, 1);
        this.tabela_transicoes.splice(indice, 1);
        this.estadosAlterados = true;
    };
    Automato.prototype.setEstadoFinal = function (nome, final) {
        var indice = this._estados.indexOf(nome);
        if (indice === -1)
            throw this.i18N.naoExisteEstado(nome);
        var indiceEstadoFinal = this._estadosFinais.indexOf(indice);
        if (final) {
            if (indiceEstadoFinal !== -1)
                throw this.i18N.estadoJaFinal(nome);
            this._estadosFinais.push(indice);
        }
        else {
            if (indiceEstadoFinal === -1)
                throw this.i18N.estadoNaoFinal(nome);
            this._estadosFinais.splice(indiceEstadoFinal, 1);
        }
        this.estadosFinaisAlterados = true;
    };
    Automato.prototype.isEstadoFinal = function (nome) {
        nome = nome || this.estado;
        if (nome === undefined)
            throw this.i18N.estadoNaoDefinido();
        var indice = this._estados.indexOf(nome);
        if (indice === -1)
            throw this.i18N.naoExisteEstado(nome);
        return this._estadosFinais.indexOf(indice) !== -1;
    };
    Automato.prototype.adicionarTransicao = function (estado, novoEstado, padrao) {
        var indiceEstado = this._estados.indexOf(estado);
        if (indiceEstado === -1)
            throw this.i18N.naoExisteEstado(estado);
        var indiceNovoEstado = this._estados.indexOf(novoEstado);
        if (indiceNovoEstado === -1)
            throw this.i18N.naoExisteEstado(novoEstado);
        var matcher = this.matcherFactory.criarMatcher(padrao);
        var transicao = new Transicao(matcher, indiceNovoEstado);
        this.tabela_transicoes[indiceEstado].push(transicao);
    };
    Automato.prototype.avancar = function (entrada) {
        if (this._estado === undefined)
            throw this.i18N.estadoNaoDefinido();
        var transicoes = this.tabela_transicoes[this._estado];
        for (var _i = 0, transicoes_1 = transicoes; _i < transicoes_1.length; _i++) {
            var transicao = transicoes_1[_i];
            if (transicao.matcher.match(entrada) !== undefined) {
                this._estado = transicao.novoEstado;
                return;
            }
        }
        throw this.i18N.nenhumaTransicao(entrada);
    };
    return Automato;
}());
exports.Automato = Automato;
var Transicao = (function () {
    function Transicao(matcher, novoEstado) {
        this.matcher = matcher;
        this.novoEstado = novoEstado;
    }
    return Transicao;
}());
var AutomatoI18NPortugues = (function () {
    function AutomatoI18NPortugues() {
    }
    AutomatoI18NPortugues.prototype.naoExisteEstado = function (estado) {
        return "N\u00E3o existe estado com o nome " + estado + ".";
    };
    AutomatoI18NPortugues.prototype.jaExisteEstado = function (estado) {
        return "J\u00E1 existe estado com o nome " + estado + ".";
    };
    AutomatoI18NPortugues.prototype.estadoJaFinal = function (estado) {
        return "Estado com o nome " + estado + " j\u00E1 \u00E9 final.";
    };
    AutomatoI18NPortugues.prototype.estadoNaoFinal = function (estado) {
        return "Estado com o nome " + estado + " n\u00E3o \u00E9 final.";
    };
    AutomatoI18NPortugues.prototype.estadoNaoDefinido = function () {
        return "Estado atual n\u00E3o definido.";
    };
    AutomatoI18NPortugues.prototype.nenhumaTransicao = function (entrada) {
        return "Nenhuma transi\u00E7\u00E3o encontrada para a entrada " + entrada + ".";
    };
    return AutomatoI18NPortugues;
}());
exports.AutomatoI18NPortugues = AutomatoI18NPortugues;
//# sourceMappingURL=automato.js.map